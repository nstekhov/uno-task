FROM adoptopenjdk/openjdk11:alpine-jre
MAINTAINER nnn
ARG JAR_FILE=target/day-counter-0.0.2-SNAPSHOT.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]