0) Check internet connection.
   Application will use port 8080. Check if this port is available.
   Make sure Docker application is up and running

1) copy source code
  git clone https://nstekhov@bitbucket.org/nstekhov/uno-task.git

2) build project
  cd uno-task
  mvn clean package

3) prepare docker image
  - signup to dockerhub

  - login into dockerhub (https://hub.docker.com):
    docker login --username=<USER_NAME> --password=<PASSWORD>

  - build docker image. Set username, repository name and version tag.
    For the example below, username "nnkhv", repository name is "uno", version tag is "0.0.2-SNAPSHOT"

    docker build -t nnkhv/uno:0.0.2-SNAPSHOT .

  - upload image to docker hub
    sudo docker push nnkhv/uno:0.0.2-SNAPSHOT

4) create aws IAM user with required permissions
  - login into AWS root account
  - open top right account drop-down and click "My Organization"
  - click on "AWS Management Console"
  - Enter "IAM" into "Find Services" text box

  - Click "Create individual IAM users"
  - Click "Manage users"
  - Click "Add user"
  - Enter "User name"
  - Select "Access type" - "Programmatic access"
  - Click "Next"
  - Select "Attach existing policies directly"
  - Enter "S3" in "Filter Policies"
  - Tick "AmazonS3FullAccess"
  - Enter "Dynamo" in "Filter Policies"
  - Tick "AmazonDynamoDBFullAccess"
  - Enter "AdministratorAccess"
  - Tick "AdministratorAccess"
  - Enter "AmazonECS_FullAccess"
  - Tick "AmazonECS_FullAccess"
  - Click "Next"
  - Click "Next: Review"
  - Click "Create User"
  - Note "User", "Access key ID" and "Secret access key"

5) Install AWS cli
  - Follow instructions on:
    https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html

6) Configure AWS cli
  - Type in terminal:
    aws configure

  - Enter required values:
      AWS Access Key ID
      AWS Secret Access Key
      Default region name
    For region name, you may select any active one, for example "ap-southeast-2".

7) Create S3 bucket to store CloudFormation config files
  - Choose bucket unique name. For below example, it is "cloudformation-3323" and
  - Set location constraint, here it is "ap-southeast-2".
  - Run creation command
      aws s3api create-bucket --bucket cloudformation-3323 --create-bucket-configuration LocationConstraint=ap-southeast-2

      On success there will be response like:

      {
          "Location": "http://cloudformation-3323.s3.amazonaws.com/"
      }

  - Note "Location" and "Bucket name".

8) Enable http access to S3 bucket files
  - Click "Services" and select "S3"
  - Click on created S3 bucket name
  - Click "Properties" tab and select "Static Web site hosting"
  - Tick "Use this bucket to host a website"
  - For "Index document" enter "index.html"
  - For "Error document" enter "error.html"
  - Click "save"

9) Adjust Template URLs and DynamoDb parameters
  - In "master.yaml" file change values "TemplateURL" tags to use S3 bucket URL we create before
    line 9, line 21, line 29, line 39, line 51, line 65,
  - In "master.yaml" file change values relate to DynamoDb
    - "AmazonDynamodbEndpoint" adjust region setting. Here region "ap-southeast-2" is set
      "https://dynamodb.ap-southeast-2.amazonaws.com"
    - "AmazonAwsAccesskey" set accessKey obtained in previous step
    - "AmazonAwsSecretkey" set secretKey obtained in previous step

10) Upload CloudFormation scripts
  - change directory to "uno-task/cloudformation/"
    cd uno-task/cloudformation/

  - upload files to s3 bucket. For the example below, bucket name is "cloudformation-3323"
    aws s3 sync --acl public-read --exclude=master.yaml . s3://cloudformation-3323


11) Create CloudFormation stack
  - change directory to "uno-task/cloudformation/"
    cd uno-task/cloudformation/

  - Choose stack name. For example below, it is "myteststack12"

  - After changing parameter, run command in terminal:
    aws cloudformation create-stack --stack-name myteststack12 --capabilities CAPABILITY_NAMED_IAM --template-body "file://./master.yaml"

  - Open in browser, Services -> CouldFormation
  - Wait until stack and nested ones become "CREATE_COMPLETE"
  - Click on created stack
  - Open "Outputs" tab
  - Note value for "DayCountServiceUrl"

12) Call endpoint
  - Install curl utility. It can be installed using brew.
  - Put value "serviceUrl". For example below, it is "myteststack14-2092513217.ap-southeast-2.elb.amazonaws.com"
  - Change parameter values "fromDate" and "toDate"
  - Use date format like "01.10.2009" (day-of-month.month-number.4-digit-year)
    curl -X GET 'http://myteststack14-2092513217.ap-southeast-2.elb.amazonaws.com:8080/day-count?fromDate=01.10.2009&toDate=05.10.2009'
  - Response is a single number shows how many days between dates

Optional: to run application locally
  - Change docker-compose settings
    - Change value for "image", set username, repository and version values:
      docker.io/nnkhv/uno:0.0.2-SNAPSHOT
    - Change value for "environment" accordingly:
      Set dynamoDb endpoint (AMAZON_DYNAMODB_ENDPOINT),
      awsAccessKey (AMAZON_AWS_ACCESSKEY),
      AwsSecretKey (AMAZON_AWS_SECRETKEY)

  - Change working directory
    cd docker

  - Start docker image
    docker-compose up

  - Call endpoint
    curl -X GET 'http://localhost:8080/day-count?fromDate=01.10.2009&toDate=05.10.2009'

Notes:
  - We would need improve controller generic exception handling
  - I decided not to specifically validate that fromDate should be before toDate,
    but it could be enforced if required
  - Endpoint can be changed to return JSON formatted result.
    For that we need to return object of result class.
  - I made decision to use modularised CouldFormation scripts but
    a downside of it is necessity of changing "TemplateURLs"
  - Endpoint name could be probably better. I would consider review comments for that.
  - Currently instance is up and running. Endpoint can be called:
    curl -X GET 'http://myteststack14-2092513217.ap-southeast-2.elb.amazonaws.com:8080/day-count?fromDate=01.10.2009&toDate=05.10.2009'
  - 'master.yaml' does not have to be uploaded. it contains access codes.