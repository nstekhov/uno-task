package au.com.uno.api;

import au.com.uno.service.DayDifferenceCalculatorService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller which handles day-count request
 */
@RestController
@RequestMapping("/")
public class DayDifferenceController {
  private final DayDifferenceCalculatorService dayDifferenceCalculatorService;

  @Autowired
  public DayDifferenceController(DayDifferenceCalculatorService dayDifferenceCalculatorService) {
    this.dayDifferenceCalculatorService = dayDifferenceCalculatorService;
  }

  @GetMapping("/day-count")
  @ResponseBody
  public Long getDayDifference(
      @RequestParam @DateTimeFormat(pattern="dd.MM.yyyy") Date fromDate,
      @RequestParam @DateTimeFormat(pattern="dd.MM.yyyy") Date toDate) {

    return dayDifferenceCalculatorService.calculateDateCount(fromDate, toDate);
  }
}
