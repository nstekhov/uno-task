package au.com.uno.service;

import au.com.uno.data.dynamodb.model.DayDifferenceResult;
import au.com.uno.data.dynamodb.repositories.DayDifferenceResultRepository;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service layer class which does calculation for how many days in between
 */
@Service
public class DayDifferenceCalculatorService {
  @Autowired
  DayDifferenceResultRepository dayDifferenceResultRepository;

  /**
   * Performs days calculation and store data in dynamoDb
   *
   * @param dateOne first date
   * @param dateTwo second date
   * @return days in between
   */
  public long calculateDateCount(Date dateOne, Date dateTwo) {
    long dayDiff = getDayCount(dateOne, dateTwo);

    DayDifferenceResult dayDifferenceResult = new DayDifferenceResult(dayDiff);
    dayDifferenceResultRepository.save(dayDifferenceResult);

    return dayDiff;
  }

  /**
   * Returns how many date between two date exclusive
   *
   * @param dateOne date one
   * @param dateTwo date two
   * @return day count
   */
  public static long getDayCount2(Date dateOne, Date dateTwo) {
    long diffMillis = Math.abs(dateTwo.getTime() - dateOne.getTime());

    if (!dateTwo.equals(dateOne)) {
      diffMillis -= Duration.ofDays(1).toMillis();
    }

    return TimeUnit.DAYS.convert(diffMillis, TimeUnit.MILLISECONDS);
  }

  public static long dayNumber(LocalDate date) {
    int a = (14 - date.getMonthValue()) / 12;
    int y = (date.getYear() + 1900) + 4800 - a;
    int m = date.getMonthValue() + 12 * a - 3;

    return date.getDayOfMonth() + (153 * m + 2)/5 + 365*y + y/4 - y/100 + y/400 - 32045;
}

  public static long getDayCount(Date dateOne, Date dateTwo) {
    LocalDate localDateOne = dateOne.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    LocalDate localDateTwo = dateTwo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

    if (localDateOne.isAfter(localDateTwo)) {
      LocalDate tmp = localDateOne;

      localDateOne = localDateTwo;
      localDateTwo = tmp;
    }

    long dayDifference = dayNumber(localDateTwo) - dayNumber(localDateOne);

    if (!localDateOne.equals(localDateTwo)) {
      dayDifference -= 1;
    }

    return dayDifference;
  }
}

