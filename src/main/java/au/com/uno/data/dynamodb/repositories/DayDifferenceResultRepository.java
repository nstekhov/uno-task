package au.com.uno.data.dynamodb.repositories;

import au.com.uno.data.dynamodb.model.DayDifferenceResult;
import java.util.Optional;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface DayDifferenceResultRepository extends CrudRepository<DayDifferenceResult, String> {
  Optional<DayDifferenceResult> findById(String id);
}
