package au.com.uno.data.dynamodb.repository;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import au.com.uno.Application;
import au.com.uno.data.dynamodb.model.DayDifferenceResult;
import au.com.uno.data.dynamodb.repositories.DayDifferenceResultRepository;
import au.com.uno.data.dynamodb.repository.rule.LocalDbCreationRule;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException;
import java.util.List;
import java.util.stream.StreamSupport;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("local")
@TestPropertySource(properties = { "amazon.dynamodb.endpoint=http://localhost:8000/", "amazon.aws.accesskey=test1", "amazon.aws.secretkey=test231" })
public class DayDifferenceResultRepositoryTest {
  @ClassRule
  public static LocalDbCreationRule dynamoDB = new LocalDbCreationRule();

  private DynamoDBMapper dynamoDBMapper;

  @Autowired
  private AmazonDynamoDB amazonDynamoDB;

  @Autowired
  DayDifferenceResultRepository repository;

  @Before
  public void setup() throws Exception {

    try {
      dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);

      CreateTableRequest tableRequest = dynamoDBMapper.generateCreateTableRequest(
          DayDifferenceResult.class);

      tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L));

      amazonDynamoDB.createTable(tableRequest);
    } catch (ResourceInUseException e) {
    }

    dynamoDBMapper.batchDelete((List<DayDifferenceResult>) repository.findAll());
  }

  @Test
  public void testResultCreatedAndCorrectlyStoredInDb() {
    DayDifferenceResult dayDifferenceResult = new DayDifferenceResult(1L);
    repository.save(dayDifferenceResult);

    Iterable<DayDifferenceResult> result = repository.findAll();

    assertThat(StreamSupport.stream(result.spliterator(), false).count(), is(greaterThan(0L)));
    assertThat(result.iterator().next().getDayCount(), is(equalTo(1L)));
  }
}
