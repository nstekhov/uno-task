package au.com.uno.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import au.com.uno.data.dynamodb.model.DayDifferenceResult;
import au.com.uno.data.dynamodb.repositories.DayDifferenceResultRepository;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DayDifferenceCalculatorServiceTest {
  @Mock
  private DayDifferenceResultRepository dayDifferenceResultRepository;

  @InjectMocks
  private DayDifferenceCalculatorService dayDifferenceCalculatorService;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testServiceLayer() {
    DateTime dt1 = new DateTime(2020, 1, 1, 12, 0, 0, 0);
    DateTime dt2 = new DateTime(2020, 1, 3, 12, 0, 0, 0);

    long dayCount =
        dayDifferenceCalculatorService.calculateDateCount(dt1.toDate(), dt2.toDate());

    assertEquals(1L, dayCount);

    ArgumentCaptor<DayDifferenceResult> dayDifferenceResultArgumentCaptor =
        ArgumentCaptor.forClass(DayDifferenceResult.class);

    verify(dayDifferenceResultRepository, times(1)).save(
        dayDifferenceResultArgumentCaptor.capture());

    assertEquals(Long.valueOf(1L), dayDifferenceResultArgumentCaptor.getValue().getDayCount());
  }

  @Test
  public void testBasic () {
    DateTime dt1 = new DateTime(2020, 1, 1, 12, 0, 0, 0);
    DateTime dt2 = new DateTime(2020, 1, 3, 12, 0, 0, 0);

    assertEquals(1, DayDifferenceCalculatorService.getDayCount(dt1.toDate(), dt2.toDate()));
  }

  @Test
  public void testTenYears () {
    DateTime dt1 = new DateTime(2020, 1, 1, 12, 0, 0, 0);
    DateTime dt2 = new DateTime(2030, 1, 1, 12, 0, 0, 0);

    assertEquals(3652, DayDifferenceCalculatorService.getDayCount(dt1.toDate(), dt2.toDate()));
  }

  @Test
  public void testSameDates () {
    DateTime dt1 = new DateTime(2020, 1, 1, 12, 0, 0, 0);
    DateTime dt2 = new DateTime(2020, 1, 1, 12, 0, 0, 0);

    assertEquals(0, DayDifferenceCalculatorService.getDayCount(dt1.toDate(), dt2.toDate()));
  }

  @Test
  public void testFromAfterTo () {
    DateTime dt1 = new DateTime(2020, 1, 3, 12, 0, 0, 0);
    DateTime dt2 = new DateTime(2020, 1, 1, 12, 0, 0, 0);

    assertEquals(1, DayDifferenceCalculatorService.getDayCount(dt1.toDate(), dt2.toDate()));
  }
}
